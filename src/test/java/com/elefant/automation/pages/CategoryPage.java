package com.elefant.automation.pages;

import com.elefant.automation.utils.MySleeper;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.Random;

/**
 * Created by master on 11/14/2017.
 */

// Firstly, this class opens the perfume category page, selects the best seller section and selects the new category and the quantity from the left menu. Secondly, it hovers over the categories and randomly selects a category and a subcategory for the selected category.

public class CategoryPage {
    @FindBy(how = How.XPATH, using = ".//*[@id='navbar-collapse-grid']/ul/li[1]/a")
    private WebElement perfumSelection;

    @FindBy(how = How.XPATH, using = ".//*[@id='navbar-1']/li[1]/a")
    private WebElement bestsellerSelection;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[1]/div[2]/div[1]/div")
    private WebElement assertBestsellerSelection;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[1]/div[1]/div/div/div[2]/div[2]/div/div/a[2]/span[1]")
    private WebElement newProductsSelection;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[1]/div[1]/div/div/div[7]/div[2]/div/div/a[3]/span[1]")
    private WebElement quantitySelection;

    @FindBy(how = How.XPATH, using = ".//*[@id='brand']")
    private WebElement brandSelection;


    //this method hovers over the categories and randomly selects a category and afterwards a subcategory
   public void categoryHover(WebDriver driver){

       Random rn = new Random();

       for(int i =0; i < 100; i++)
       {
           int answer = rn.nextInt(7) + 1;
           Actions act= new Actions(driver);
           WebElement category = driver.findElement(By.xpath(".//*[@id='navbar-collapse-grid']/ul/li[" + String.valueOf(answer) + "]/a"));
           act.moveToElement(category).build().perform();

           for(int j =0; i < 100; i++) {
               int subcategory = rn.nextInt(5) + 1;
               WebElement subCategory = driver.findElement(By.xpath(".//*[@id='navbar-collapse-grid']/ul/li[" + String.valueOf(answer) + "]/div/div/div["+ String.valueOf(subcategory) + "]/a[1]"));
               subCategory.click();
           }
       }

    }

    // this method clicks the perfume category, selects the best seller subcategory. Then, from the left menu, selects the new products option and the quantity
    public void perfumCategoryTest(WebDriver driver){
       Actions act = new Actions(driver);
       act.moveToElement(perfumSelection).build().perform();
       bestsellerSelection.click();
       MySleeper.mySleeper(2000);

       Assert.assertEquals(assertBestsellerSelection.getText(), " Cosmetice si Parfumuri > Parfumuri");

       newProductsSelection.click();
       MySleeper.mySleeper(2000);
       quantitySelection.click();
       MySleeper.mySleeper(2000);
    }

    /*this method selects a perfume from a certain brand, i.e. Adidas
    *
    * @param yourBrand is the selected brand */
    public void perfumBrandSelection(String yourBrand){
        brandSelection.clear();
        brandSelection.sendKeys(yourBrand);
        MySleeper.mySleeper(2000);
    }
}
