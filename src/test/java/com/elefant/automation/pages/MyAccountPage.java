package com.elefant.automation.pages;

import com.elefant.automation.utils.MySleeper;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.Random;

/**
 * Created by master on 11/9/2017.
 */

//this method clicks the already existing addresses section and adds a new address, by randomly selecting a county and a city. Then, it clicks the orders section and opens an order to see the details
public class MyAccountPage {
    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[3]/div[1]/div[2]/div[2]/a")
    private WebElement myAddress;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[3]/div[3]/div/div[1]/div[1]/div")
    private WebElement assertMyAddress;

    @FindBy(how = How.CLASS_NAME, using = "user-button-add")
    private WebElement newAddress;

    @FindBy(how = How.XPATH, using = ".//*[@id='my_address_new']/div/div/div/form/div[1]/div/div/textarea")
    private WebElement newAddressDetail;

    @FindBy(how = How.CLASS_NAME, using = "user-button-cancel")
    private WebElement cancelAddress;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[1]/div[2]/div[3]/a")
    private WebElement myOrders;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[3]/div/div[1]/div/div")
    private WebElement assertMyOrders;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[3]/div/div[3]/div[3]/div/div[10]/a")
    private WebElement myOrderDetails;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[3]/div/div[1]/div[1]/div")
    private WebElement assertMyOrderDetails;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/header/div[1]/div[4]/div[4]/a/div/div[2]")
    private WebElement myAccountButton;

    @FindBy(how = How.CLASS_NAME, using = "not_logged_in")
    private WebElement enterAccount;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/header/div[1]/div[4]/div[4]/ul/li[6]/a")
    private WebElement details;

    @FindBy(how = How.XPATH, using = ".//*[@id='my_address_new']/div/div/div/form/div[3]/div/div/select")
    private WebElement county;

    @FindBy(how = How.XPATH, using = ".//*[@id='my_address_new']/div/div/div/form/div[3]/div/div/select")
    private WebElement city;

    //this method hovers over my account icon and selects the account details
    public void myDetails(WebDriver driver) {
        Actions accountDetail = new Actions(driver);
        accountDetail.click(myAccountButton).build().perform();
        details.click();
    }

    /* this method sets the new address in the address field

    @param address is the new address

    */
    public void myAccountChange(String address){
        myAddress.click();

        Assert.assertEquals(assertMyAddress.getText(), "Adresele mele");

        newAddress.click();
        newAddressDetail.clear();
        newAddressDetail.sendKeys(address);
    }

    //this method hovers over the city field (from the new address window) to randomly select a city for the new address
    public void citySelection(WebDriver driver){
        Actions cityDetail = new Actions(driver);
        cityDetail.click(city).build().perform();
        MySleeper.mySleeper(2000);
        Random rn = new Random();

        for(int i =0; i < 100; i++) {
            int answer = rn.nextInt(20) + 1;
            WebElement citySelection = driver.findElement(By.xpath(".//*[@id='change_region_new']/option[" + String.valueOf(answer) + "]"));
            citySelection.click();
        }
    }

    //this method hovers over the county field (from the new address window) to randomly select a county for the new address
    public void countySelection(WebDriver driver){
        Actions addressDetail = new Actions(driver);
        addressDetail.click(county).build().perform();
        MySleeper.mySleeper(2000);

        Random rn = new Random();

        for(int i =0; i < 100; i++) {
            int answer = rn.nextInt(20) + 1;
            WebElement countySelection = driver.findElement(By.xpath(".//*[@id='my_address_new']/div/div/div/form/div[3]/div/div/select/option[" + String.valueOf(answer) + "]"));
            countySelection.click();
        }
    }

    //this method clicks the cancel address button
    public void cancelAddress(){
        cancelAddress.click();
    }

    //this method opens the order section and then opens the details for an order
    public void myOrders(){
        myOrders.click();

        Assert.assertEquals(assertMyOrders.getText(), "Comenzile mele");

        myOrderDetails.click();

        Assert.assertEquals(assertMyOrderDetails.getText(), "Detalii comanda #9484722");
    }
}
