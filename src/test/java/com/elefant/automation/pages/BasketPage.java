package com.elefant.automation.pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by master on 11/14/2017.
 */

//this class deals with the basket: adds a product to the basket, modifies the quantity, saves the changes and then empties the basket.

public class BasketPage {
    @FindBy(how = How.CLASS_NAME, using = "product-adauga-in-cos-text")
    private WebElement addToBasket;

    @FindBy(how = How.XPATH, using = ".//*[@id='cart_count']")
    private WebElement myBasket;

    @FindBy(how = How.XPATH, using = ".//*[@id='main_comanda']/div[1]/div/h3")
    private WebElement assertMyBasket;

    @FindBy(how = How.XPATH, using = ".//*[@id='more_338067_1']/a")
    private WebElement quantityChange;

    @FindBy(how = How.XPATH, using = ".//*[@id='more_338067_1_actions']/div[2]/a[2]/span")
    private WebElement moreQuantity;

    @FindBy(how = How.XPATH, using = ".//*[@id='more_338067_1_actions']/div[3]/a[1]")
    private WebElement saveQuantity;

    @FindBy(how = How.XPATH, using = ".//*[@id='338067']")
    private WebElement deleteBasket;

    /* this method does the following:

    adds a product to the basket
    loads the basket
    changes the product quantity from one to two
    saves the new quantity
    empties the basket
    */

    public void addToBasket(){
        addToBasket.click();
        myBasket.click();

        Assert.assertEquals(assertMyBasket.getText(), "Sumar comanda");

        quantityChange.click();
        moreQuantity.click();
        saveQuantity.click();
        deleteBasket.click();
    }
}
