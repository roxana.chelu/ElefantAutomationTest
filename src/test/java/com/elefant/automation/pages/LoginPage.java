package com.elefant.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

import static com.elefant.automation.utils.MySleeper.mySleeper;

/**
 * Created by master on 11/7/2017.
 */

//this class identifies the login and registration fields
public class LoginPage {
    @FindBy(how = How.ID, using = "login_username")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "login_password")
    private WebElement passwordField;

    @FindBy(how = How.ID, using = "login_classic")
    private WebElement submitButton;

    @FindBy(how = How.CLASS_NAME, using = "error_input")
    private List<WebElement> errors;

    @FindBy(how = How.CLASS_NAME, using = "radio-inline")
    private WebElement genderType;

    @FindBy(how = How.ID, using = "r_first_name")
    private WebElement firstName;

    @FindBy(how = How.ID, using = "r_last_name")
    private WebElement lastName;

    @FindBy(how = How.ID, using = "r_email")
    private WebElement email;

    @FindBy(how = How.ID, using = "r_password")
    private WebElement password;

    @FindBy(how = How.ID, using = "r_c_password")
    private WebElement confirmPassword;

    @FindBy(how = How.ID, using = "newsletter_subscribe")
    private WebElement subscribeCheckbox;

    @FindBy(how = How.ID, using = "terms_ok")
    private WebElement termsCheckbox;

    @FindBy(how = How.ID, using = "register_classic")
    private WebElement newAccountSubmit;

    @FindBy(how = How.CSS, using = ".hidden-xs.header-account-display")
    private List<WebElement>myAccountButton;

    @FindBy(how = How.CLASS_NAME, using = "not_logged_in")
    private WebElement enterAccount;

    @FindBy(how = How.CLASS_NAME, using = "logout-button")
    private WebElement logout;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/header/div[1]/div[4]/div[4]/ul/li[3]/a")
    private WebElement createAccount;


    /*this method asserts the login errors
    
    @param userError is the error received for incorrect user
    @param passError is the error received for incorrect password
    @param GeneralError is the general error for incorrect user and password
    */
    public boolean checkErrors(String userError, String passError, String generalError) {
        for (WebElement we : errors) {
            System.out.println("Class list-ustyled text: " + we.getText());
        }

        if (emailField.getText().length() > 0 && passwordField.getText().length() > 0) {
            return errors.size()==0;
        }
        if (emailField.getText().length() ==0) {
            if (passwordField.getText().length() == 0) {
                return errors.get(0).getText().compareTo(userError)==0 && errors.get(1).getText().compareTo(passError)==0;

            }
            else {
                return errors.get(0).getText().compareTo(userError)==0;
            }
        }
        else {
            if (passwordField.getText().length() == 0) {
                return errors.get(0).getText().compareTo(passError) == 0;
            }
        }
        return false;
    }

    /*this method asserts the registration errors
    
    @param fNameError is the error received for first name
    @param lNameError is the error received for last name
    @param emailError is the error received for email
    @param passError is the error received for password
    @param generalError is the general error
    */
    public boolean registrationCheckErrors(String fNameError, String lNameError, String emailError, String passError, String generalError) {
        for (WebElement we : errors) {
            System.out.println("Class list-ustyled text: " + we.getText());
        }

        if (email.getText().length() > 0 && password.getText().length() > 0 && firstName.getText().length()>0 && lastName.getText().length()>0) {
            return errors.size()==0;
        }
        if (email.getText().length() ==0) {
            if (password.getText().length() == 0) {
                return errors.get(0).getText().compareTo(passError)==0 && errors.get(1).getText().compareTo(passError)==0;

            }
            else {
                return errors.get(0).getText().compareTo(emailError)==0;
            }
        }
        else {
            if (firstName.getText().length() == 0) {
                return errors.get(0).getText().compareTo(fNameError) == 0;
            }
        }
        return false;
    }

    //this method hovers over my account icon (right corner) and clicks log in
    public void hover(WebDriver driver){
        Actions act = new Actions(driver);
        act.click(myAccountButton.get(0)).build().perform();
        enterAccount.click();
    }



    //this method hovers over my account icon (right corner) and clicks log out
    public void logout(WebDriver driver){
        Actions actLog = new Actions(driver);
        actLog.click(myAccountButton.get(myAccountButton.size()-1)).build().perform();
        logout.click();
    }


    /* this method sends keys for the user and password to log in

    @param user is the user’s email address
    @param password is the user’s password
    */
    public void login(String user, String password){
        emailField.clear();
        emailField.sendKeys(user);
        passwordField.clear();
        passwordField.sendKeys(password);
        submitButton.submit();
    }

    // this method hover over my account icon (right corner) and selects create new account
    public void createAccount(WebDriver driver){
        Actions act = new Actions(driver);
        act.click(myAccountButton.get(1)).build().perform();
        createAccount.click();

    }


    /*this method creates a new account, by setting the first name, last name, email address and password. In addition, it selects the gender and checks the subscribe and terms&conditions checkboxes.

    @param fname is the first name
    @param lname is the last name
    @param userEmail is the user’s email
    @param pass is the new password
    @param confPass is the confirmed password
    */
    public void registration(String fName, String lName, String userEmail, String pass, String confPass) {

        for (int i = 0; i < 2 + (int) Math.random() * 2; i++) {
            genderType.click();
        }
        firstName.clear();
        firstName.sendKeys(fName);
        lastName.clear();
        lastName.sendKeys(lName);
        email.clear();
        email.sendKeys(userEmail);
        password.clear();
        password.sendKeys(pass);
        confirmPassword.clear();
        confirmPassword.sendKeys(confPass);
        subscribeCheckbox.click();
        termsCheckbox.click();
        newAccountSubmit.submit();

    }
}
