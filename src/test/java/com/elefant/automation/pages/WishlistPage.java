package com.elefant.automation.pages;

import com.elefant.automation.utils.MySleeper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by master on 11/14/2017.
 */

//the class identifies the add to wishlist button, selects the wishlist and adds the product. Then it empties the wishlist
public class WishlistPage {
    @FindBy(how = How.CLASS_NAME, using = "ajax-complete")
    private WebElement wishlistButton;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div/div[2]/div/div[5]/div[8]/ul/li[3]")
    private WebElement selectWishlist;

    @FindBy(how = How.CLASS_NAME, using = "header-wishlist-icon")
    private WebElement myWishlist;

    @FindBy(how = How.CLASS_NAME, using = "fa fa-times-circle")
    private WebElement deleteWishlist;

    //the method opens my Wishlist by hovering over My Profile and selecting my wishlist and then deletes the created wishlist
    public void myWishlist(WebDriver driver){
        Actions act = new Actions(driver);
        act.click(myWishlist).build().perform();
        MySleeper.mySleeper(2000);
        deleteWishlist.click();
    }

    //this method adds a product to the wishlist, by clicking the wishlist button after searching for a certain product
    public void addToWishlist(WebDriver driver){
        Actions act = new Actions(driver);
        act.click(wishlistButton).build().perform();
        selectWishlist.submit();
    }
}
