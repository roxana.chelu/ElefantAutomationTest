package com.elefant.automation.pages;

/**
 * Created by master on 11/9/2017.
 */

//this class sets the user and the password for the login test

public class LoginModelPage {
    private String username;
    private String password;

    //this method gets the user name
    public String getUsername() {
        return username;
    }

    //this method sets the user name
    public void setUsername(String username) {
        this.username=username;
    }

    //this method gets the password
    public String getPassword() {
        return password;
    }

    //this method sets the password
    public void setPassword(String password) {
        this.password= password;
    }
}
