package com.elefant.automation.pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by master on 11/14/2017.
 */


//this class opens the book details, Oamenii fericiti citesc si beau cafea, to be used in other tests for not repeating the code
public class ProductSelection {
    @FindBy(how = How.XPATH, using = ".//*[@id='100338067']/div/div[2]/div/div[1]/a")
    private WebElement bookSelection;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div/div[2]/div/div[5]/h1")
    private WebElement assertBookSelection;

    //the method clicks the book details.
    public void bookSelection(){
        bookSelection.click();

       Assert.assertEquals(assertBookSelection.getText(), "Oamenii fericiti citesc si beau cafea");
    }
}
