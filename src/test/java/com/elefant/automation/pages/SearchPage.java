package com.elefant.automation.pages;

import com.elefant.automation.utils.MySleeper;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.Random;

/**
 * Created by master on 11/9/2017.
 */

//this class uses the search field to search for a product and then randomly sorts the product and selects the price
public class SearchPage {
    @FindBy(how = How.XPATH, using = ".//*[@id='query']")
    private WebElement searchField;

    @FindBy(how = How.XPATH, using = ".//*[@id='searchsubmit']")
    private WebElement searchSubmit;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[1]/div[2]/div[1]/div/h1/span[1]")
    private WebElement assertSearch;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div[2]/div/a")
    private WebElement deleteFilter;

    @FindBy(how = How.CLASS_NAME, using = ".slider-handle.min-slider-handle.round")
    private WebElement slider;

    @FindBy(how = How.CLASS_NAME, using = "html/body/div[4]/div[1]/div[1]/div[2]/div[2]/div/div[1]/div/div[1]/div[3]/a/span[2]")
    private WebElement sortingSection;

    //this method randomly sorts the searched product using the sort field
    public void sorting(WebDriver driver) {
        Actions act = new Actions(driver);
        act.click(sortingSection).build().perform();

        Random rn = new Random();

        for(int i =0; i < 100; i++)
        {
            int answer = rn.nextInt(4) + 1;
            WebElement sortingItem = driver.findElement(By.xpath(".//*[@id='elf-sort-by-list']/li[" + String.valueOf(answer) + "]/a"));
            sortingItem.click();
        }
    }

    //this method changes the price bar for the searched product
    public void sliderHover(WebDriver driver){
        Actions move = new Actions(driver);
        move.clickAndHold(slider).moveByOffset(50, 50).build().perform();
        MySleeper.mySleeper(4000);
    }

    /* this method searches for a certain product, i.e. myOption

     @param myOption is the searched product
    */
    public void mySearch(String myOption){
        searchField.clear();
        searchField.sendKeys(myOption);
        searchSubmit.submit();
        MySleeper.mySleeper(2000);

        Assert.assertEquals(assertSearch.getText(), "Rezultate pentru \"oamenii fericiti citesc si beau cafea\"");
    }

    //this method deletes the searching filters, by clicking delete filters option.
    public void deleteFilters(){
        deleteFilter.click();
    }
}
