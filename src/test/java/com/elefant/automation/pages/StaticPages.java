package com.elefant.automation.pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by master on 11/14/2017.
 */

//this class asserts the static pages, it clicks the page and then checks the results.
public class StaticPages {
    @FindBy(how = How.XPATH, using = "html/body/div[4]/div/footer/div[2]/div[2]/ul/li[1]/a")
    private WebElement aboutUsPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[2]/div[1]/p[2]/span")
    private WebElement assertAboutUsPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[5]/div[1]/ul[1]/li[3]/a")
    private WebElement authorsPage;

    @FindBy(how = How.XPATH, using = ".//*[@id='content_cat']/div[1]/span")
    private WebElement assertAuthorsPage;

    @FindBy(how = How.XPATH, using = ".//*[@id='content_cat']/div[7]/div[1]/ul[2]/li[3]/a")
    private WebElement confidentialityPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[2]/h3[23]/strong")
    private WebElement assertConfidentialityPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[5]/div[1]/ul[1]/li[6]/a")
    private WebElement contactPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[2]/div/span[1]/span")
    private WebElement assertContactPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[5]/div[1]/ul[3]/li[2]/a")
    private WebElement deliveryPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[2]/p[54]/strong[1]/span")
    private WebElement assertDeliveryPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[5]/div[1]/ul[2]/li[2]/a")
    private WebElement termsAndConditionsPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[2]/h2")
    private WebElement assertTermsAndConditionsPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[5]/div[1]/ul[3]/li[10]/a")
    private WebElement questionsPage;

    @FindBy(how = How.XPATH, using = "html/body/main/div/section[2]/div/div/h2")
    private WebElement assertQuestionsPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[5]/div[1]/ul[1]/li[4]/a")
    private WebElement publishingHousePage;

    @FindBy(how = How.XPATH, using = ".//*[@id='content_cat']/div[1]/span")
    private WebElement assertPublishingHousePage;

    @FindBy(how = How.XPATH, using = ".//*[@id='content_cat']/div[9]/div[1]/ul[2]/li[5]/a")
    private WebElement regulationPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[2]/div/ul/li[1]/span/a")
    private WebElement assertRegulationPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[5]/div[1]/ul[3]/li[5]/a")
    private WebElement returnPolicyPage;

    @FindBy(how = How.XPATH, using = "html/body/div[6]/div[2]/div/div[2]/div[2]/p[68]/strong[1]/span")
    private WebElement assertReturnPolicyPage;


    //this method clicks the About Us page
    public void aboutUsPage(){
        aboutUsPage.click();

        Assert.assertEquals(assertAboutUsPage.getText(), "elefant online S.A.");
    }

    // this method clicks the Authors page
    public void authorsPage(){
        authorsPage.click();

        Assert.assertEquals(assertAuthorsPage.getText(), "Autori");

    }

    // this method clicks the Confidentiality page
    public void confidentialityPage(){
        confidentialityPage.click();

        Assert.assertEquals(assertConfidentialityPage.getText(), "5. Prelucrarea datelor cu caracter personal");

    }

    // this method clicks the Contact page
    public void contactPage(){
        contactPage.click();

        Assert.assertEquals(assertContactPage.getText(), "Cod Unic de Inregistrare: RO 26396066");

    }

    // this method clicks the Delivery page
    public void deliveryPage(){
        deliveryPage.click();

        Assert.assertEquals(assertDeliveryPage.getText(), "Metode de livrare");

    }

    // this method clicks the Terms and Conditions page
    public void termsAndConditionsPage(){
        termsAndConditionsPage.click();

        Assert.assertEquals(assertTermsAndConditionsPage.getText(), "Termeni si conditii");
    }

    // this method clicks the Frequent Questions page
    public void frequentQuestionsPage(){
        questionsPage.click();

        Assert.assertEquals(assertQuestionsPage.getText(), "Activitate recentă");

    }


    // this method clicks the Publishing house page
    public void publishingHousePage(){
        publishingHousePage.click();

        Assert.assertEquals(assertPublishingHousePage.getText(), "Edituri");

    }

    // this method clicks the Regulation page
    public void regulationPage(){

        regulationPage.click();

        Assert.assertEquals(assertRegulationPage.getText(), "Regulament Cel mai mic pret online. Platim de 2X diferenta");


    }

    // this method clicks the Return Policy page
    public void returPolicyPage(){
        returnPolicyPage.click();

        Assert.assertEquals(assertReturnPolicyPage.getText(), "Cum pot returna un produs/ o comanda?");

    }
}
