package com.elefant.automation.pages;

import com.elefant.automation.utils.MySleeper;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by master on 11/9/2017.
 */

//this class identifies the Google Plus Authentication button and the email and password fields to log in on the website.

public class AuthenticationPage {
    @FindBy(how = How.CLASS_NAME, using = "login-google-button")
    private WebElement googlePlusbutton;

    @FindBy(how = How.XPATH, using = ".//*[@id='headingText']")
    private WebElement assertGoogleEmailField;

    @FindBy(how = How.XPATH, using = ".//*[@id='identifierId']")
    private WebElement googleEmailField;

    @FindBy(how = How.CLASS_NAME, using = "RveJvd snByac")
    private WebElement nextButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='password']/div[1]/div/div[1]/input")
    private WebElement passwordField;



    /* this method clicks the Google Plus button and then tries to log in using an email and a password

    @param email is the email used to log in

    @param password is the log in password

    */
    public void authentication(String email, String password){
        googlePlusbutton.click();

        Assert.assertEquals(assertGoogleEmailField.getText(), "Sign in");

        googleEmailField.clear();
        googleEmailField.sendKeys(email);
        MySleeper.mySleeper(2000);
        nextButton.click();
        MySleeper.mySleeper(2000);
        passwordField.clear();
        passwordField.sendKeys(password);
        MySleeper.mySleeper(2000);
        nextButton.click();

    }
}
