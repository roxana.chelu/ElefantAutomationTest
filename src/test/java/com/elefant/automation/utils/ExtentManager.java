package com.elefant.automation.utils;

import com.relevantcodes.extentreports.ExtentReports;

/**
 * Created by master on 11/16/2017.
 */
 
 //this class is used for reports
public class ExtentManager {
    private static ExtentReports extent;
    
    /*this method is used to syncronize the reports
    
    @param filePath is the file path
    */
    public synchronized static ExtentReports getReporter(String filePath) {
        if (extent == null) {
            extent = new ExtentReports(filePath, true);

            extent
                    .addSystemInfo("Host Name", "Roxana")
                    .addSystemInfo("Environment", "QA");
        }

        return extent;
    }
}
