package com.elefant.automation.utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;

/**
 * Created by master on 11/23/2017.
 */
 
 //this class is used to take screenshots with the test and to save them in the screenshots folder
public class Screenshots{
    public static void screenshots(WebDriver driver){
        try{
            File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            File finalFile = new File ("src\\main\\resources\\screenshots\\" + System.currentTimeMillis() +".png");
            FileUtils.copyFile(screenshotFile, finalFile);
        } catch (Exception ex){
            System.out.print("Unable to do a screenshot");
        }
    }
}
