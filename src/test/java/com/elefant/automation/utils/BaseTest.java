package com.elefant.automation.utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;
import java.io.*;
//import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by master on 11/7/2017.
 */
 /*this class dose the following:
 1.starts the driver
 2. closes the driver
 3. deletes the screenshots before starting a test
 4. creates the reports
 */
public class BaseTest {
    public WebDriver driver = null;
    public String siteAddress = null;

    protected ExtentReports extent;
    protected ExtentTest test;

    final String filePath = "Extent.html";

    WebBrowser.Browsers browserType;

    /* this method starts the driver and sets the browser to elefant.ro by taking the information from pom.xml before the test
    
    @param website is the tested website
    
    @param url is the tested website taken from pom.xml
    */
    @Parameters("url")
    @BeforeTest
    public void startDriver(String website) {
        siteAddress = website;
        try {
            driver = WebBrowser.getDriver(WebBrowser.Browsers.FIREFOX);
        }
        catch (Exception ex) {
            System.out.println("Browser specified is not on the supported browser list! Please use firefox, chrome, ie or htmlunit as argument!");
        }
    }

    //this method deletes the screenshots before starting the test and taking other screenshots
    @BeforeTest
    public void deleteScreenshots(){
        File here = new File("src\\main\\resources\\screenshots");
        System.out.println(here.getAbsolutePath());
        try {
            File [] pngFiles = here.listFiles(new FileFilter() {
                public boolean accept(File file) {
                    return file.isFile() && file.getName().toLowerCase().endsWith(".png");
                }
            });
            for (File f : pngFiles) {
                f.delete();
                System.out.println("Deleting screenshots");
            }
            }
         catch(Exception e){
            e.printStackTrace();
        }
    }


    //this method closes the driver
    @AfterTest
    public void closeDriver() {
        try {
            driver.close();
        }
        catch (Exception ex) {
            System.out.println("No driver was to close!");
        }
    }

    /*this method creates the reports by displaying if the test passed or failed
    
    @param result is the test result
    */
    @AfterMethod
    protected void afterMethod(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
           // test.log(LogStatus.FAIL, result.getThrowable());
            test.log(LogStatus.FAIL, "Test failed");
        } else if (result.getStatus() == ITestResult.SKIP) {
           // test.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
            test.log(LogStatus.SKIP, "Test skipped ");
        } else {
            test.log(LogStatus.PASS, "Test passed");
        }

        extent.endTest(test);
        extent.flush();
    }

    //this method sets the report
    @BeforeSuite
    public void beforeSuite() {
        extent = ExtentManager.getReporter(filePath);
    }

    //this method closes the report
    @AfterSuite
    protected void afterSuite() {
        extent.close();
    }
}
