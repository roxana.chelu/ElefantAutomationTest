package com.elefant.automation.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by master on 11/7/2017.
 */
 
 //this class sets up the browsers
public class WebBrowser {
    public enum Browsers {
        FIREFOX,
        CHROME,
        IE,
        HTMLUNIT
    }
    
    /*this method selects the broswer
    
    @param browser is the browser used for testing
    */
    public static WebDriver getDriver(Browsers browser) throws Exception {
        WebDriver driver = null;

        switch (browser) {
            case IE:
            {
                System.setProperty("webdriver.ie.driver", "C:\\Users\\master\\Desktop\\elefantAutomationTest\\src\\test\\java\\com\\elefant\\automation\\drivers\\IEDriverServer32.exe");
                return new InternetExplorerDriver();
            }
            case CHROME:
            {
                System.setProperty("webdriver.chrome.driver", "C:\\Users\\master\\Desktop\\elefantAutomationTest\\src\\test\\java\\com\\elefant\\automation\\drivers\\chromedriver.exe");

                ChromeOptions options = new ChromeOptions();
                options.addArguments("start-maximized");
                return new ChromeDriver(options);
            }
            case FIREFOX:
            {
                System.setProperty("webdriver.gecko.driver", "C:\\Users\\master\\Desktop\\elefantAutomationTest\\src\\test\\java\\com\\elefant\\automation\\drivers\\geckodriver.exe");
                ProfilesIni profile = new ProfilesIni();
                FirefoxProfile myprofile = profile.getProfile("default");


                DesiredCapabilities dc = new DesiredCapabilities();
                dc.setCapability(FirefoxDriver.PROFILE, myprofile);
                dc.setCapability("browserName","safari");

                return new FirefoxDriver(dc);
            }
            case HTMLUNIT:
            {
                return new HtmlUnitDriver();
            }
            default:
            {
                throw new Exception("Invalid browser");
            }
        }
    }
}
