package com.elefant.automation.tests.myAccountTest;

import com.elefant.automation.pages.LoginPage;
import com.elefant.automation.pages.MyAccountPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 11/9/2017.
 */

//this class tests the account, by changing the delivery address and checking the previous ordered products
public class MyAccountTest extends BaseTest {
    @DataProvider(name = "myAccountProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"Piata Unirii"});
        return dp.iterator();
    }

    /* The steps are:

        navigate to the page
        hover over My account and log in
        open the account details
        open the previous orders and select an order to see the details
        change the address name
        change the city, by randomly selecting a city from the bar
        change the county, by randomly selecting a county from the list
        cancel the newly added address

        @param address is the new address

    */
    @Test(dataProvider = "myAccountProvider")
    public void myAccountTest(String address) {
        test = extent.startTest("myAccountTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(2000);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }


        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.hover(driver);

        MySleeper.mySleeper();

        loginPage.login("roxana.chelu@yahoo.com", "alabala87");

        MySleeper.mySleeper(2000);

        MyAccountPage myAccount = PageFactory.initElements(driver, MyAccountPage.class);
        myAccount.myDetails(driver);

        MySleeper.mySleeper();
        myAccount.myOrders();

        myAccount.myAccountChange(address);

        MySleeper.mySleeper(2000);

        myAccount.citySelection(driver);

        myAccount.countySelection(driver);

        myAccount.cancelAddress();

        Screenshots.screenshots(driver);

    }
}
