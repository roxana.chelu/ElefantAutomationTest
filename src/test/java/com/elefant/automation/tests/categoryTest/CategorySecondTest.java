package com.elefant.automation.tests.categoryTest;

import com.elefant.automation.pages.CategoryPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 11/14/2017.
 */

// this class tests the perfume category
public class CategorySecondTest extends BaseTest{
    @DataProvider(name = "CategoryProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"adidas"});
        return dp.iterator();
    }

    /*the steps in this method are:

    navigate to the site
    click the perfume category
    click the best seller subcategory
    from the left menu, select the new product and quantity options
    select an Adidas perfume

    @param yourBrand is the searched perfume brand

    */
    @Test(dataProvider = "CategoryProvider")
    public void categoryTest(String yourBrand) {
        test = extent.startTest("myCategorySecondTest");

        driver.navigate().to(siteAddress);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }

        MySleeper.mySleeper(3000);
        CategoryPage categorySelection = PageFactory.initElements(driver, CategoryPage.class);
        categorySelection.perfumCategoryTest(driver);
        MySleeper.mySleeper(3000);
        categorySelection.perfumBrandSelection(yourBrand);
        MySleeper.mySleeper(3000);

        Screenshots.screenshots(driver);
    }
}
