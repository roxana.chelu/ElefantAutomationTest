package com.elefant.automation.tests.categoryTest;

import com.elefant.automation.pages.CategoryPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Created by master on 11/14/2017.
 */

//this class tests the categories
public class CategoryTest extends BaseTest {

    /*the steps in this method are:

    hover over categories and randomly select one
    hover over a subcategory and randomly select one

    */
    @Test
    public void categoryTest(){
        test = extent.startTest("myCategoryTest");

        driver.navigate().to(siteAddress);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }

        MySleeper.mySleeper(2000);
        CategoryPage categorySelection = PageFactory.initElements(driver, CategoryPage.class);
        categorySelection.categoryHover(driver);

        Screenshots.screenshots(driver);
    }
}
