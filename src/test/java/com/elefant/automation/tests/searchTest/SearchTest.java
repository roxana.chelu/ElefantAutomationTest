package com.elefant.automation.tests.searchTest;

import com.elefant.automation.pages.SearchPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 11/9/2017.
 */

// this class tests the search bar
public class SearchTest extends BaseTest{
    @DataProvider(name = "SearchProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"monopoly"});
        dp.add(new String[]{"ceas"});
        return dp.iterator();
    }


    /* The steps are:

        navigate to the website
        search for the product
        randomly sort the products
        change the price slider
        delete the filter

        @param yourOption is the searched product

    */
    @Test(dataProvider = "SearchProvider")
    public void mySearchTest(String yourOption){
        test = extent.startTest("mySearchTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(2000);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }

        SearchPage mySearchCriteria = PageFactory.initElements(driver, SearchPage.class);
        MySleeper.mySleeper();
        mySearchCriteria.mySearch(yourOption);

        mySearchCriteria.sorting(driver);

        mySearchCriteria.sliderHover(driver);

        mySearchCriteria.deleteFilters();

        Screenshots.screenshots(driver);

    }
}
