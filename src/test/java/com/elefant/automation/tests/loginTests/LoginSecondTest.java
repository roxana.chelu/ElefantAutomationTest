package com.elefant.automation.tests.loginTests;

import com.elefant.automation.pages.LoginModelPage;
import com.elefant.automation.pages.LoginPage;
import com.elefant.automation.reader.ExcelReader;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Created by master on 11/9/2017.
 */

//this class tests the log in page by taking the data, user and password, from an excel document.
public class LoginSecondTest  extends BaseTest{
    @DataProvider(name = "ExcelDataProvider")
    public Object[][] excelDataProviderCollection() throws Exception {
        ClassLoader cl = getClass().getClassLoader();
        File excelFile = new File(cl.getResource("src\\main\\resources\\datasources\\loginData.xlsx").getFile());
        String[][] excelData = ExcelReader.readExcelFile(excelFile, "Sheet1", true, true);
        Object [] [] dp = new Object[excelData.length][1];

        for (int i = 0; i < dp.length; i++) {
            LoginModelPage account = new LoginModelPage();
            account.setUsername(excelData[i][0]);
            account.setPassword(excelData[i][1]);
            dp[i][0] = account;
        }
        return dp;
    }

    //this method tests the login page using the user and password from the excel data provider
    @Test(dataProvider = "ExcelDataProvider")
    public void loginTestExcel(LoginModelPage lm) {
        test = extent.startTest("myLoginSecondTest");

        driver.navigate().to(siteAddress);
        MySleeper.mySleeper(2000);
        LoginPage lp = PageFactory.initElements(driver, LoginPage.class);

        lp.login(lm.getUsername(),lm.getPassword());
    }
}
