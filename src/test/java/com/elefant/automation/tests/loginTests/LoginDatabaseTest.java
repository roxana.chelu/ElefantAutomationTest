package com.elefant.automation.tests.loginTests;

import com.elefant.automation.pages.LoginPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.sql.*;

/**
 * Created by master on 11/14/2017.
 */



// this class uses data from an SQL database to test the log in
public class LoginDatabaseTest extends BaseTest{
    ExtentReports ex =  new ExtentReports("D:\\report.html", false);

    /*this method connects the sql database to Selenium
    
    @param username is the database user
    @param password is the database password
    @param database is the database from were the log in credentials are taken
    */
    @Parameters({"username", "password", "database"})
    @Test(groups = {"GDB"})
    public void databaseTest1(String username, String password, String database) {
        test = extent.startTest("myLoginDatabaseTest");

        Reporter.log("Before login\n");

        ExtentTest t1 = ex.startTest("DB_test");
        System.out.println("Login with user" + username + " pass " + password + " on DB "+ database);
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" +database+ "?useSSL=false&serverTimezone=UTC", username, password);
            Reporter.log("Connection established\n");
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery("Select * from login_credentials;");
            while (res.next()) {
                System.out.println(res.getString("email") + " "+ res.getString("password"));
                Reporter.log(res.getString("email") + " "+ res.getString("password"));
            }
            res.close();
            conn.close();
            Reporter.log("Connection ended\n");
            t1.log(LogStatus.PASS,"Connection ended successfully");
        }
        catch (SQLException sexc) {
            sexc.printStackTrace();
            t1.log(LogStatus.FAIL,"Connection has not been successfully!");
        }
        ex.endTest(t1);
        ex.flush();
    }

    //this method takes the email and the password from the database to log in on the website
    @Test
    public void databaseTest() {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/siit_aut?useSSL=false&serverTimezone=UTC", "root", "Ilavayou");
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery("Select * from login_credentials;");
            while (res.next()) {
                System.out.println(res.getString("email") + " "+ res.getString("password"));

                System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
                WebDriver driver = new FirefoxDriver();
                driver.navigate().to(siteAddress);

                LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
                MySleeper.mySleeper(2000);
                loginPage.login(res.getString("email"), res.getString("password"));
            }
            res.close();
            conn.close();
        }
        catch (SQLException sexc) {
            sexc.printStackTrace();
        }

    }
}
