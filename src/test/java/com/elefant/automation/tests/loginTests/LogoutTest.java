package com.elefant.automation.tests.loginTests;

import com.elefant.automation.pages.LoginPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 11/9/2017.
 */
//the class tests the log out option
public class LogoutTest extends BaseTest{

/* The steps in this method are:

    navigate to the website
    hover over My account to log in
    log in using the given credentials
    log out

*/

    @Test
    public void logout(){
        test = extent.startTest("myLogoutTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(2000);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();

        }catch(Exception ex){
              System.out.println("There is no promotion page!");
            }



        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.hover(driver);

        MySleeper.mySleeper();
        loginPage.login("roxana.chelu@yahoo.com", "alabala87");

        MySleeper.mySleeper(2000);

        loginPage.logout(driver);

        Screenshots.screenshots(driver);

    }
}
