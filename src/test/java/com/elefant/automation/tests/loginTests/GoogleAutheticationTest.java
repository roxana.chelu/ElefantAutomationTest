package com.elefant.automation.tests.loginTests;

import com.elefant.automation.pages.AuthenticationPage;
import com.elefant.automation.pages.LoginPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 11/9/2017.
 */

//this class tests the authentication using google plus
public class GoogleAutheticationTest extends BaseTest{
    @DataProvider(name = "LoginDataProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"roxmariac87", "alabala87"});
        return dp.iterator();
    }

    /* The steps are:
    hover over my profile to log in
    click the Google Plus icon
    authenticate using an email and password

    @email is the email used to log in
    @password is the password used to log in
    */
    @Test(dataProvider = "LoginDataProvider")
    public void authenticationTest(String email, String password){
        test = extent.startTest("myGoogleAuthenticationTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(2000);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }

        LoginPage hoverAccount= PageFactory.initElements(driver, LoginPage.class);
        hoverAccount.hover(driver);

        AuthenticationPage authentication= PageFactory.initElements(driver, AuthenticationPage.class);
        authentication.authentication(email, password);

        Screenshots.screenshots(driver);
    }
}
