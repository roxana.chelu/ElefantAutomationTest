package com.elefant.automation.tests.loginTests;

import com.elefant.automation.pages.LoginPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 11/9/2017.
 */

//this class is used to test the registration page
public class RegistrationTest extends BaseTest{
    @DataProvider(name = "LoginDataProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"firstName", "", "","",""});
        dp.add(new String[]{"firstName", "lastName", "","",""});
        dp.add(new String[]{"firstName", "lastName", "email","",""});
        dp.add(new String[]{"firstName", "lastName", "email","pass",""});
        dp.add(new String[]{"firstName", "lastName", "email","pass","pass"});
        dp.add(new String[]{"", "lastName", "","",""});
        dp.add(new String[]{"", "lastName", "email","",""});
        dp.add(new String[]{"", "lastName", "","pass",""});
        dp.add(new String[]{"", "lastName", "","","pass"});
        dp.add(new String[]{"", "lastName", "email","pass",""});
        dp.add(new String[]{"", "lastName", "email","","pass"});
        dp.add(new String[]{"", "lastName", "email","pass","pass"});
        dp.add(new String[]{"", "", "email","",""});
        dp.add(new String[]{"", "", "email","pass",""});
        dp.add(new String[]{"", "", "email","pass","pass"});
        dp.add(new String[]{"", "", "","pass",""});
        dp.add(new String[]{"", "", "","pass","pass"});
        return dp.iterator();
    }

/* The steps in this method are:

    hovers over My account and selects create a new account
    populates the fields to create an account

    @param fname is the first name
    @param lname is the last name
    @param email is the email address
    @param pass is the new password
    @param confirmP is the confirmed password

    */

    @Test(dataProvider = "LoginDataProvider")
    public void registrationTest(String fname, String lname, String email, String pass, String confirmP){
        test = extent.startTest("myRegistrationTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(2000);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.createAccount(driver);
        MySleeper.mySleeper(2000);
        loginPage.registration(fname, lname, email,pass,confirmP);

        Screenshots.screenshots(driver);
    }
}
