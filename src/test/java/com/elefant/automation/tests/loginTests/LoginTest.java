package com.elefant.automation.tests.loginTests;

import com.elefant.automation.pages.LoginPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 11/9/2017.
 */

//this class tests the log in page
public class LoginTest extends BaseTest{
    @DataProvider(name = "LoginDataProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"", ""});
        dp.add(new String[]{"email", ""});
        dp.add(new String[]{"", "password"});
        dp.add(new String[]{"email", "password"});
        dp.add(new String[]{"roxana.chelu@yahoo.com", "alabala87"});
        return dp.iterator();
    }

    /* this method checks the log in using the credentials provided by the data provider. It first hovers over My account and then tries to log in.
    
    @param email is the email used to log in
    @param password is the user’s password
    */
    @Test(dataProvider = "LoginDataProvider")
    public void loginThirdTest(String email, String password) {
        test = extent.startTest("myLoginTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(2000);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }

        MySleeper.mySleeper(2000);

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.hover(driver);
        MySleeper.mySleeper();
        loginPage.login(email, password);

        Screenshots.screenshots(driver);
    }
}
