package com.elefant.automation.tests.basketAndWishlistTests;

import com.elefant.automation.pages.BasketPage;
import com.elefant.automation.pages.ProductSelection;
import com.elefant.automation.pages.SearchPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 11/14/2017.
 */

// this class tests the basket
public class BasketTest extends BaseTest {

    /*The steps in this method are:

    navigate to the site
    search for the book: Oamenii fericiti citesc si beau cafea using the SearchPage
    select the book using ProductSelection Page
    add the product to the basket

    */
    @Test
    public void myBasketTest() {
        test = extent.startTest("myBasketTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(2000);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }

        SearchPage mySearchCriteria = PageFactory.initElements(driver, SearchPage.class);
        MySleeper.mySleeper();
        mySearchCriteria.mySearch("oamenii fericiti citesc si beau cafea");

        ProductSelection myProduct = PageFactory.initElements(driver, ProductSelection.class);
        MySleeper.mySleeper();
        myProduct.bookSelection();

        BasketPage myBasket =  PageFactory.initElements(driver, BasketPage.class);
        MySleeper.mySleeper();
        myBasket.addToBasket();

        Screenshots.screenshots(driver);
    }
}