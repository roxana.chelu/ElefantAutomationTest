package com.elefant.automation.tests.basketAndWishlistTests;

import com.elefant.automation.pages.LoginPage;
import com.elefant.automation.pages.ProductSelection;
import com.elefant.automation.pages.SearchPage;
import com.elefant.automation.pages.WishlistPage;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by master on 11/14/2017.
 */

//the class tests the wishlist
public class WishlistTest extends BaseTest {

    /* The steps in this method are:

        navigate to the site
        hovers to My account icon and clicks log in
        log in using valid credentials
        search for the book: Oamenii fericiti citesc si beau cafea using the SearchPage
        select the book using ProductSelection Page
        add  the book to the wishlist and select the wishlist name
        delete the new wishlist

    */
    @Test
    public void myWishlistTest() {
        test = extent.startTest("myWishlistTest");

        driver.navigate().to(siteAddress);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }

        MySleeper.mySleeper(2000);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.hover(driver);
        MySleeper.mySleeper(2000);
        loginPage.login("roxana.chelu@yahoo.com", "alabala87");
        MySleeper.mySleeper(2000);

        SearchPage mySearchCriteria = PageFactory.initElements(driver, SearchPage.class);
        MySleeper.mySleeper();
        mySearchCriteria.mySearch("oamenii fericiti citesc si beau cafea");

        ProductSelection myProduct = PageFactory.initElements(driver, ProductSelection.class);
        MySleeper.mySleeper(2000);
        myProduct.bookSelection();

        MySleeper.mySleeper(2000);
        WishlistPage myWishlist = PageFactory.initElements(driver, WishlistPage.class);
        myWishlist.addToWishlist(driver);
        MySleeper.mySleeper(2000);
        myWishlist.myWishlist(driver);

        Screenshots.screenshots(driver);
    }
}
