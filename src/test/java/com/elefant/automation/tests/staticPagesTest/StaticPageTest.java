package com.elefant.automation.tests.staticPagesTest;

import com.elefant.automation.pages.StaticPages;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by master on 11/16/2017.
 */

//this class tests the static pages
public class StaticPageTest extends BaseTest{

    //the method clicks the About Us page
    @Test
    public void aboutUsTest(){
        test = extent.startTest("aboutUsTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages aboutUs = PageFactory.initElements(driver, StaticPages .class);
        aboutUs.aboutUsPage();

        Screenshots.screenshots(driver);
    }

    //the method clicks the Authors page
    @Test
    public void authorsTest(){
        test = extent.startTest("authorsTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages authors = PageFactory.initElements(driver, StaticPages .class);
        authors.authorsPage();

        Screenshots.screenshots(driver);
    }

    //the method clicks the Confidentiality page
    @Test
    public void confidentialityTest(){
        test = extent.startTest("confidentialityTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages confidentiality = PageFactory.initElements(driver, StaticPages .class);
        confidentiality.confidentialityPage();

        Screenshots.screenshots(driver);
    }

    //the method clicks the Contact page
    @Test
    public void contactTest(){
        test = extent.startTest("contactTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages contact = PageFactory.initElements(driver, StaticPages .class);
        contact.contactPage();

        Screenshots.screenshots(driver);
    }

    //the method clicks the Delivery page
    @Test
    public void deliveryTest(){
        test = extent.startTest("deliveryTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages delivery = PageFactory.initElements(driver, StaticPages .class);
        delivery.deliveryPage();

        Screenshots.screenshots(driver);
    }

    //the method clicks the Frequent Questions page
    @Test
    public void frequentQuestionsTest(){
        test = extent.startTest("frequentQuestionsTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages questions = PageFactory.initElements(driver, StaticPages .class);
        questions.frequentQuestionsPage();
    }

    //the method clicks the Publishing House page
    @Test
    public void publishingHouseTest(){
        test = extent.startTest("publishingHouseTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages publishingHouse = PageFactory.initElements(driver, StaticPages .class);
        publishingHouse.publishingHousePage();

        Screenshots.screenshots(driver);
    }

    //the method clicks the Regulation page
    @Test
    public void regulationTest(){
        test = extent.startTest("regulationTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages regulations = PageFactory.initElements(driver, StaticPages .class);
        regulations.regulationPage();

        Screenshots.screenshots(driver);
    }


    //the method clicks the Return Policy page
    @Test
    public void returnPolicyTest(){
        test = extent.startTest("returnPolicyTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages returnPolicy = PageFactory.initElements(driver, StaticPages .class);
        returnPolicy.returPolicyPage();

        Screenshots.screenshots(driver);
    }

    //the method clicks the Terms and Conditions page
    @Test
    public void termsAndConditionsTest(){
        test = extent.startTest("termsAndConditionsTest");

        driver.navigate().to(siteAddress);

        MySleeper.mySleeper(3000);
        StaticPages terms = PageFactory.initElements(driver, StaticPages .class);
        terms.termsAndConditionsPage();

        Screenshots.screenshots(driver);
    }
}
