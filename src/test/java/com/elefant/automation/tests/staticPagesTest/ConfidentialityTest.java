package com.elefant.automation.tests.staticPagesTest;

import com.elefant.automation.pages.StaticPages;
import com.elefant.automation.utils.BaseTest;
import com.elefant.automation.utils.MySleeper;
import com.elefant.automation.utils.Screenshots;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by master on 11/14/2017.
 */

//the class tests the static page Confidentiality
public class ConfidentialityTest extends BaseTest {

    //the method clicks the page
    @Test
    public void staticPagesTest(){
        test = extent.startTest("confidentialityTest");

        driver.navigate().to(siteAddress);

        try{
            WebElement backtosite = driver.findElement(By.xpath(".//*[@id='up']/div/div[1]/a"));
            backtosite.click();
        }catch(Exception ex){
            System.out.println("There is no promotion page!");
        }

        MySleeper.mySleeper(3000);
        StaticPages confidentiality = PageFactory.initElements(driver, StaticPages .class);
        confidentiality.confidentialityPage();

        Screenshots.screenshots(driver);
    }
}
